package tsc.abzalov.tm;

import org.junit.Test;

/**
 * Тесты основного класса приложения.
 * @author Ruslan Abzalov.
 */
public class ApplicationTest {

    /**
     * Тест запуска приложения без аргументов командной строки.
     */
    @Test
    public void zeroArgsTest() {
        Application.main(null);
        System.out.println();
    }

    /**
     * Тест запуска приложения с аргументом командной строки <code>help</code>.
     */
    @Test
    public void helpArgTest() {
        Application.main("help");
        System.out.println();
    }

    /**
     * Тест запуска приложения с аргументом командной строки <code>version</code>.
     */
    @Test
    public void versionArgTest() {
        Application.main("version");
        System.out.println();
    }

    /**
     * Тест запуска приложения с аргументом командной строки <code>about</code>.
     */
    @Test
    public void aboutArgTest() {
        Application.main("about");
        System.out.println();
    }

    /**
     * Тест запуска приложения с неподдерживаемым аргументом командной строки.
     */
    @Test
    public void incorrectArgTest() {
        Application.main("someCommand");
        System.out.println();
    }

}